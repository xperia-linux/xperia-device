# Master device file for Xperia.
# The product make files include this file

# Might as well squeeze out some performance
TARGET_GLOBAL_CFLAGS=+"-mcpu=arm1136j-s"

PRODUCT_MANUFACTURER:=port

PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.hwrotation=180 # Fixes the flipped screen

#Device specific init script
#The 'x' in init.x.rc must be the first word of the 'Machine' field
#found in /proc/cpuinfo

PRODUCT_COPY_FILES := \
    device/xperia/init.htc.rc:root/init.htc.rc
