#Inherit the Xperia device configuration
$(call inherit-product, device/xperia/device_xperia.mk)
#Inherit the generic product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/generic.mk)

PRODUCT_NAME := generic_xperia
PRODUCT_DEVICE := xperia
PRODUCT_MODEL := Android on Xperia
